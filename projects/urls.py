from django.urls import path

from . import views

urlpatterns = [
    path("api/project/", views.ProjectListCreate.as_view(), name="project"),
    path(
        "api/project/list",
        views.ProjectViewSet.as_view({"get": "list"}),
        name="project-list",
    ),
    path(
        "api/project/<int:pk>",
        views.ProjectViewSet.as_view({"get": "retrieve"}),
        name="project-detail",
    ),
    path(
        "api/project/create", views.ProjectCreateView.as_view(), name="project-create"
    ),
    path(
        "api/project/<int:pk>/delete",
        views.ProjectCreateView.as_view(),
        name="project-create",
    ),
    path("api/user/", views.UserListCreate.as_view(), name="user"),
    path("api/user/list", views.UserViewSet.as_view({"get": "list"}), name="user-list"),
    path(
        "api/user/<int:pk>",
        views.UserViewSet.as_view({"get": "retrieve"}),
        name="user-detail",
    ),
]
