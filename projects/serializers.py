from rest_framework import serializers

from .models import Project, User


class ProjectSerializer(serializers.ModelSerializer):
    """
    Serialize Project model to JSON.
    """

    users = serializers.StringRelatedField(many=True)

    def get_users(self, project):
        qs = User.objects.filter(projects=project)
        return UserSerializer(qs, many=True).data

    class Meta:
        model = Project
        fields = ("id", "key", "name", "users")


class UserSerializer(serializers.ModelSerializer):
    """
    Serialize User model to JSON.
    """

    class Meta:
        model = User
        fields = (
            "name_first",
            "name_last",
            "email",
            "date_joined",
            "date_left",
            "projects",
        )

    def validate(self, data):
        """
        Check that the date_joined before date_left.
        """
        if "date_left" in data and "date_joined" in data:
            if data["date_joined"] > data["date_left"]:
                raise serializers.ValidationError(
                    {"date_left": "Date left must be before date joined."}
                )
        return data
