from django.core import validators
from django.db import models


class Project(models.Model):
    """
    Model containing Project details.
    """

    key = models.CharField(
        max_length=32,
        unique=True,
        validators=(
            validators.RegexValidator(
                regex=r"^[a-z0-9_]*$", message='Allowed characters: "a-z", "0-9", "_"'
            ),
        ),
    )
    name = models.CharField(max_length=512, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return str(self.name)


class User(models.Model):
    """
    Model containing User details, linked to Projects.
    """

    name_first = models.CharField(
        max_length=128,
        validators=(
            validators.RegexValidator(
                regex=r"^[a-zA-Z]*$", message="Text characters only."
            ),
        ),
    )
    name_last = models.CharField(
        max_length=128,
        validators=(
            validators.RegexValidator(
                regex=r"^[a-zA-Z]*$", message="Text characters only."
            ),
        ),
    )
    email = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        validators=(validators.EmailValidator(message="Email not valid."),),
    )
    date_joined = models.DateField()
    date_left = models.DateField(blank=True, null=True)
    projects = models.ManyToManyField("Project", blank=True, related_name="users")
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["name_last"]

    def __str__(self):
        return f"{self.name_last}, {self.name_first} |"
