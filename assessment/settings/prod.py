import os

from .base import *

SECRET_KEY = os.getenv("SECRET_KEY")

ALLOWED_HOSTS = [os.getenv("HOST_URL")]

REST_FRAMEWORK = {
    "DEFAULT_RENDERER_CLASSES": ("rest_framework.renderers.JSONRenderer",)
}
