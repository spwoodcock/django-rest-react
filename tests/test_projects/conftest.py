import pytest
from model_bakery import baker


def uubb():
    """
    Wrapper func for unfilled_user_bakery_batch.
    """

    def unfilled_user_bakery_batch(n):
        """
        Create a batch of mocked data for User model.
        Optional fields are left empty.

        Parameters
        ----------
        n : int
            Number of records to create.

        Returns
        -------
        uubb : Django User model object queryset.
        """
        uubb = baker.make(
            "projects.User",
            _fill_optional=[
                "email",
                "date_left",
            ],
            _quantity=n,
        )
        return uubb

    return unfilled_user_bakery_batch


@pytest.fixture
def fubb():
    """
    Wrapper func for filled_user_bakery_batch.
    """

    def filled_user_bakery_batch(n):
        """
        Create a batch of mocked data for User model.
        Optional fields are filled.

        Parameters
        ----------
        n : int
            Number of records to create.

        Returns
        -------
        fubb : Django User model object queryset.
        """
        fubb = baker.make("projects.User", _quantity=n)
        return fubb

    return filled_user_bakery_batch


@pytest.fixture
def fub():
    """
    Wrapper func for filled_user_bakery.
    """

    def filled_user_bakery():
        """
        Create a single mocked object of User model.
        Optional fields are filled

        Returns
        -------
        fub : Django User model object.
        """
        fub = baker.make("projects.User", projects=baker.make("projects.Project"))
        return fub

    return filled_user_bakery
