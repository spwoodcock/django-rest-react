from datetime import datetime

import factory
import pytest
from rest_framework import serializers

from projects.serializers import ProjectSerializer, UserSerializer
from tests.test_projects.factories import ProjectFactory, UserFactory


class TestProjectSerializer:
    @pytest.mark.unit
    def test_serialize_model(self):
        project = ProjectFactory.build()
        serializer = ProjectSerializer(project)

        assert serializer.data

    @pytest.mark.unit
    @pytest.mark.django_db
    def test_serialized_data(self, mocker):
        valid_serialized_data = factory.build(dict, FACTORY_CLASS=ProjectFactory)

        serializer = ProjectSerializer(data=valid_serialized_data)

        assert serializer.is_valid()
        assert serializer.errors == {}


class TestUserSerializer:
    @pytest.mark.unit
    def test_serialize_model(self):
        user = UserFactory.build()
        serializer = UserSerializer(user)

        assert serializer.data

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_serialized_data(self, mocker):
        valid_serialized_data = factory.build(dict, FACTORY_CLASS=UserFactory)

        serializer = UserSerializer(data=valid_serialized_data)

        assert serializer.is_valid()
        assert serializer.errors == {}

    @pytest.mark.unit
    def test_date_validation_fail(self):
        invalid_date_left_record = factory.build(
            dict,
            FACTORY_CLASS=UserFactory,
            date_joined=datetime.strptime("01012019", "%m%d%Y").date(),
            date_left=datetime.strptime("01012018", "%m%d%Y").date(),
        )

        with pytest.raises(serializers.ValidationError):
            serializer = UserSerializer(data=invalid_date_left_record)
            serializer.validate(data=invalid_date_left_record)
