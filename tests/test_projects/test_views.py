import json

import factory
import pytest
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import reverse
from django_mock_queries.mocks import MockSet

from projects.models import Project, User
from projects.views import ProjectViewSet, UserViewSet
from tests.test_projects.factories import ProjectFactory, UserFactory

pytestmark = [pytest.mark.urls("assessment.urls"), pytest.mark.unit]


class TestProjectViewSet:
    def test_list(self, mocker, rf):
        # Arrange
        url = reverse("project-list")
        request = rf.get(url)
        qs = MockSet(
            ProjectFactory.build(), ProjectFactory.build(), ProjectFactory.build()
        )
        view = ProjectViewSet.as_view({"get": "list"})
        # Mock
        mocker.patch.object(ProjectViewSet, "get_queryset", return_value=qs)
        # Act
        response = view(request).render()
        # Assert
        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    @pytest.mark.django_db
    def test_create(self, mocker, rf):
        valid_data_dict = factory.build(dict, FACTORY_CLASS=ProjectFactory)
        url = reverse("project-list")
        request = rf.post(
            url,
            content_type="application/json",
            data=json.dumps(valid_data_dict, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(Project, "save")
        view = ProjectViewSet.as_view({"post": "create"})

        response = view(request).render()

        assert response.status_code == 201
        assert json.loads(response.content) == valid_data_dict

    @pytest.mark.django_db
    def test_retrieve(self, mocker, rf):
        project = ProjectFactory.create()
        expected_json = {
            "key": project.key,
            "name": project.name,
        }
        url = reverse("project-detail", kwargs={"pk": project.id})
        request = rf.get(url)
        mocker.patch.object(
            ProjectViewSet, "get_queryset", return_value=MockSet(project)
        )
        view = ProjectViewSet.as_view({"get": "retrieve"})

        response = view(request, pk=project.id).render()

        assert response.status_code == 200
        assert json.loads(response.content) == expected_json

    @pytest.mark.django_db
    def test_update(self, mocker, rf):
        old_project = ProjectFactory.create()
        new_project = ProjectFactory.build()
        project_dict = {
            "key": new_project.key,
            "name": new_project.name,
        }
        url = reverse("project-detail", kwargs={"pk": old_project.id})
        request = rf.put(
            url,
            content_type="application/json",
            data=json.dumps(project_dict, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(ProjectViewSet, "get_object", return_value=old_project)
        mocker.patch.object(Project, "save")
        view = ProjectViewSet.as_view({"put": "update"})

        response = view(request, pk=old_project.id).render()

        assert response.status_code == 200
        assert json.loads(response.content) == project_dict

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "field",
        [
            ("key"),
            ("name"),
        ],
    )
    def test_partial_update(self, mocker, rf, field):
        project = ProjectFactory.create()
        project_dict = {
            "key": project.key,
            "name": project.name,
        }
        valid_field = project_dict[field]
        url = reverse("project-detail", kwargs={"pk": project.id})
        request = rf.patch(
            url,
            content_type="application/json",
            data=json.dumps({field: valid_field}, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(ProjectViewSet, "get_object", return_value=project)
        mocker.patch.object(Project, "save")
        view = ProjectViewSet.as_view({"patch": "partial_update"})

        response = view(request).render()

        assert response.status_code == 200
        assert json.loads(response.content)[field] == valid_field

    @pytest.mark.django_db
    def test_delete(self, mocker, rf):
        project = ProjectFactory.create()
        url = reverse("project-detail", kwargs={"pk": project.id})
        request = rf.delete(url)
        mocker.patch.object(ProjectViewSet, "get_object", return_value=project)
        del_mock = mocker.patch.object(Project, "delete")
        view = ProjectViewSet.as_view({"delete": "destroy"})

        response = view(request).render()

        assert response.status_code == 204
        assert del_mock.assert_called


class TestUserViewSet:
    def test_list(self, mocker, rf):
        url = reverse("user-list")
        request = rf.get(url)
        qs = MockSet(UserFactory.build(), UserFactory.build(), UserFactory.build())
        mocker.patch.object(UserViewSet, "get_queryset", return_value=qs)
        view = UserViewSet.as_view({"get": "list"})

        response = view(request).render()

        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    @pytest.mark.django_db
    def test_create(self, mocker, rf):
        valid_data_dict = factory.build(dict, FACTORY_CLASS=UserFactory)
        valid_data_dict["date_joined"] = valid_data_dict["date_joined"].strftime(
            "%Y-%m-%d"
        )
        valid_data_dict["date_left"] = valid_data_dict["date_left"].strftime("%Y-%m-%d")
        url = reverse("user-list")
        request = rf.post(
            url,
            content_type="application/json",
            data=json.dumps(valid_data_dict, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(User, "save")
        view = UserViewSet.as_view({"post": "create"})

        response = view(request).render()

        response_json = json.loads(response.content)
        response_json.pop("projects", None)

        assert response.status_code == 201
        assert response_json == valid_data_dict

    @pytest.mark.django_db
    def test_retrieve(self, mocker, rf):
        user = UserFactory.create()
        expected_json = {
            "name_first": user.name_first,
            "name_last": user.name_last,
            "email": user.email,
            "date_joined": user.date_joined.strftime("%Y-%m-%d"),
            "date_left": user.date_left.strftime("%Y-%m-%d"),
            "projects": list(user.projects.values_list(flat=True)),
        }

        url = reverse("user-detail", kwargs={"pk": user.id})
        request = rf.get(url)
        mocker.patch.object(UserViewSet, "get_queryset", return_value=MockSet(user))
        view = UserViewSet.as_view({"get": "retrieve"})

        response = view(request, pk=user.id).render()

        print("Hello")
        print(expected_json)
        print("Hello")
        print(json.loads(response.content))
        print("Hello")

        assert response.status_code == 200
        assert json.loads(response.content) == expected_json

    @pytest.mark.django_db
    def test_update(self, mocker, rf):
        old_user = UserFactory.create()
        new_user = UserFactory.create()
        user_json = {
            "name_first": new_user.name_first,
            "name_last": new_user.name_last,
            "email": new_user.email,
            "date_joined": new_user.date_joined.strftime("%Y-%m-%d"),
            "date_left": new_user.date_left.strftime("%Y-%m-%d"),
            "projects": list(new_user.projects.values_list(flat=True)),
        }
        url = reverse("user-detail", kwargs={"pk": old_user.id})
        request = rf.put(
            url,
            content_type="application/json",
            data=json.dumps(user_json, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(UserViewSet, "get_object", return_value=old_user)
        mocker.patch.object(User, "save")
        view = UserViewSet.as_view({"put": "update"})

        response = view(request, pk=old_user.id).render()

        assert response.status_code == 200
        assert json.loads(response.content) == user_json

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "field",
        [
            ("name_first"),
            ("name_last"),
            ("email"),
            ("date_joined"),
            ("date_left"),
            ("projects"),
        ],
    )
    def test_partial_update(self, mocker, rf, field):
        user = UserFactory.create()
        user_json = {
            "name_first": user.name_first,
            "name_last": user.name_last,
            "email": user.email,
            "date_joined": user.date_joined.strftime("%Y-%m-%d"),
            "date_left": user.date_left.strftime("%Y-%m-%d"),
            "projects": list(user.projects.values_list(flat=True)),
        }
        valid_field = user_json[field]
        url = reverse("user-detail", kwargs={"pk": user.id})
        request = rf.patch(
            url,
            content_type="application/json",
            data=json.dumps({field: valid_field}, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(UserViewSet, "get_object", return_value=user)
        mocker.patch.object(User, "save")
        view = UserViewSet.as_view({"patch": "partial_update"})

        response = view(request).render()

        assert response.status_code == 200
        assert json.loads(response.content)[field] == valid_field

    @pytest.mark.django_db
    def test_delete(self, mocker, rf):
        user = UserFactory.create()
        url = reverse("user-detail", kwargs={"pk": user.id})
        request = rf.delete(url)
        mocker.patch.object(UserViewSet, "get_object", return_value=user)
        del_mock = mocker.patch.object(User, "delete")
        view = UserViewSet.as_view({"delete": "destroy"})

        response = view(request).render()

        assert response.status_code == 204
        assert del_mock.assert_called
